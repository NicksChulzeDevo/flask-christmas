# flask-christmas

This project is for everyone, who wants to give someone their heart (which won't be given away next year). Basically, this is my first flask prototype with animation and pico.css as CSS-Framework (which is fine for everything else that I am not doing here).   


## Getting started

### Built With
The following mayor frameworks/tools have been used
* [Flask](https://flask.palletsprojects.com/en/2.0.x/)
* [Gunicorn](https://gunicorn.org/)
* PicoCss


## Installation

1. Clone the repo
```sh
git clone https://gitlab.com/NicksChulzeDevo/flask-christmas
cd vaccelerate-api
```

2. Run the script below within the directory to create the virtual environment:

```sh
python3 -m venv venv
```

3. In order to start the virtual environment run the script below:

```sh
.\venv\Scripts\activate
```

4. Install python packages

```sh
pip install -r requirements.txt
```

<!-- USAGE EXAMPLES -->
## Usage

1. Activate the virtual environment, run the script below:

```sh
.\venv\Scripts\activate
```

2. Go into the folder and run the flask server:

```sh
set FLASK_APP=flaskr
flask run
```

## License
For open source projects, say how it is licensed.
