import json
from datetime import date, datetime, timedelta
import random
import sys
from flask.json import dump
from markupsafe import escape
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, current_app
)



bp = Blueprint('main', __name__)


@bp.route("/")
def main():
    with open("config.json", encoding="UTF-8") as f:
        config = json.load(f)

    doors = config["doors"]
    random.shuffle(doors, lambda: .354)
    return render_template('main.html', doors=doors, title=config["title"], card_content=config["card-content"])

@bp.route("/imprint")
def imprint():
    return render_template('imprint.html')

@bp.route("/<int:door_number>")
def door(door_number):
    # load config
    with open("config.json") as f:
        config = json.load(f)

    doors = config["doors"]
    data = [d for d in doors if d['door_number'] == door_number][0]

    # check date for validity
    today = date.today()
    start_date = datetime.strptime(config["calendar-start"], '%d-%m-%Y')
    door_date = start_date + timedelta(days=door_number-1)
    is_locked = door_date.date() > today
    print(today, start_date, door_date, is_locked)

    if not is_locked:
        for i, door in enumerate(config["doors"]):
            if door["door_number"] == data["door_number"]:
                config["doors"][i]["is_open"] = True

        with open('config.json', 'w') as f:
            json.dump(config, f, ensure_ascii=False)

    return render_template('door.html', post=data, is_locked=is_locked)


@bp.errorhandler(404)
def page_not_found(error):
    return redirect(url_for('main'))
